//
//  ViewController.swift
//  constraints
//
//  Created by Macbook Pro on 2016-03-02.
//  Copyright © 2016 Macbook Pro. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NSXMLParserDelegate {

    @IBOutlet weak var tf: UITextField!
    
    @IBOutlet weak var Accounttf: UITextField!
    
    @IBOutlet weak var Passwordtf: UITextField!
    
    @IBOutlet weak var accountlbl: UILabel!
    
    @IBOutlet weak var emaillbl: UILabel!
    
    var parser = NSXMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var route = NSMutableString()
    var timestamp = NSMutableString()
    
    func beginParsing()
    {
        posts = []
        parser = NSXMLParser(contentsOfURL:(NSURL(string:"henrikbjorklund.ddns.net:782/teed/api_caller.php"))!)!
        parser.delegate = self
        parser.parse()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Jjaaj
        let time = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-M-d H:m"
        
        let rounded = SkipToEvenFiveMinutes(time)
        let formatteddate = formatter.stringFromDate(rounded)
        
        accountlbl.text = formatteddate
        
        let dateHash = String.sha1(formatteddate)
        emaillbl.text = dateHash()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var second : SecondVC = segue.destinationViewController as! SecondVC
      
    }
    
    func Login()
    {
        let myUrl = NSURL(string:"henrikbjorklund.ddns.net:782/teed/api_caller.php");
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "Post";
       
        let email = tf.text!
        let account = Accounttf.text!
        let password = Passwordtf.text!
        let version = "1.1.3"
        let client = "ios"
        
        
        let postString = "&username=\(email)&userhash=\(password)"
        
        
        
        
    }
    func skip(date: NSDate) -> NSDate {
        let componentMask : NSCalendarUnit = ([NSCalendarUnit.Year , NSCalendarUnit.Month , NSCalendarUnit.Day , NSCalendarUnit.Hour ,NSCalendarUnit.Minute])
        let components = NSCalendar.currentCalendar().components(componentMask, fromDate: date)
        
        if(components.second < 150)
        {
            components.minute -= 5 + components.minute % 5
            components.second = 0
        }
        else{
        components.minute += 5 - components.minute % 5
        components.second = 0
        }
        
        if (components.minute == 0) {
            components.hour += 1
            
        }
        
        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }
    
    /*
    
    func date() -> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-M-d H:m"
        let tiden = dateFormatter.stringFromDate(NSDate())
        
        let dateSha1 = String.sha1(tiden)()
        
        return dateSha1
    }
    */
    
    func SkipToEvenFiveMinutes(date:NSDate) -> NSDate!
    {
        
        let componentMask : NSCalendarUnit = ([NSCalendarUnit.Year , NSCalendarUnit.Month , NSCalendarUnit.Day , NSCalendarUnit.Hour ,NSCalendarUnit.Minute, NSCalendarUnit.Second])
        let components = NSCalendar.currentCalendar().components(componentMask, fromDate: date)
        let current = components.minute * 60
        let currentSeconds = current + components.second
      
       
        
        let diff = currentSeconds % (60 * 5);
        let difference = Double(diff)
        if (difference > 60.0 * 2.5) {
            components.second += 60 * 5 - diff
        } else {
            components.second -= diff
        }
        
        return NSCalendar.currentCalendar().dateFromComponents(components)!
        
        
    }
}
extension String {
    
    
    func sha1() -> String {
        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
        var digest = [UInt8](count:Int(CC_SHA1_DIGEST_LENGTH), repeatedValue: 0)
        CC_SHA1(data.bytes, CC_LONG(data.length), &digest)
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joinWithSeparator("")
    }
    


}


